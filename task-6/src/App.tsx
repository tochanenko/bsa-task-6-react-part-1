import React from 'react';
import './App.css';
import Header from './components/header';
import Footer from './components/footer';
import Chat from './components/chat';
import MessagesProvider from './components/providers/MessagesProvider';
import JsonProvider from './components/providers/JsonProvider';

class App extends React.Component<{}, { loaded: boolean }> {

    constructor(props: any) {
        super(props);

        this.state = {
            loaded: false
        }
    }

    componentDidMount() {
        JsonProvider.getMessages('https://edikdolynskyi.github.io/react_sources/messages.json').then(
            res => {
                MessagesProvider.getInstance().setMessages(res);
                this.setState({ loaded: true });
            }
        );
    }

    render() {
        const loaded: boolean = this.state.loaded;
        return loaded ? (
            <div>
                <Header />
                <Chat />
                <Footer />
            </div>
        ) : <span>Loading...</span>
    }
}

export default App;
