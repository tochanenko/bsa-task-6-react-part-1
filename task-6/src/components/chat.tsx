import React from 'react';
import '../resources/styles/chat.css';
import SingleMessage from './interfaces/SingleMessage';
import MessagesProvider from './providers/MessagesProvider';
import MessageCompose from '../components/message-compose';
import ChatHeader from '../components/chat-header';
import JsonProvider from './providers/JsonProvider';

class Chat extends React.Component {

    private monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    constructor(props: any) {
        super(props);
    }

    private messages = MessagesProvider.getInstance();

    changeLiked(e: React.MouseEvent, i: number) {
        this.messages.changeLiked(i);
        this.forceUpdate();
    }

    deleteMessage(e: React.MouseEvent, i: number) {
        this.messages.deleteMessage(i);
        this.forceUpdate();
    }

    editMessage(e: React.MouseEvent, i: number) {
        var newText = window.prompt("Write new message:", this.messages.getMessage(i).text);
        this.messages.editMessage(i, newText!, new Date().toLocaleString());
        this.forceUpdate();
    }

    createMessages = () => {
        let messagesList = [];

        let lastDate = -100;
        let lastMonth = "";

        for (let i = 0; i < this.messages.getMessagesLength(); i++) {

            let currentFullDate = new Date(this.messages.getMessage(i).editedAt);

            let currentDate = currentFullDate.getDate();
            let currentMonth = this.monthNames[currentFullDate.getMonth()];

            let currentMinutes = currentFullDate.getMinutes();
            let currentHours = currentFullDate.getHours();

            if (currentDate != lastDate || currentMonth != lastMonth) {
                lastDate = currentDate;
                lastMonth = currentMonth;

                messagesList.push(
                    <div className="date-line">{lastDate} {lastMonth}</div>
                );
            }

            if (this.messages.getMessage(i).user == 'ME') {
                messagesList.push(
                    <div className="message-block byme">
                        <div className="messages-actions">
                            <img src={this.messages.getMessage(i).avatar} />
                            <span className="message-user-name">{this.messages.getMessage(i).user}</span>
                            <span className="message-date">{ currentHours > 10 ? currentHours : '0' + currentHours }:{ currentMinutes > 10 ? currentMinutes : '0' + currentMinutes }</span>
                            <button className="action" onClick={(e) => this.deleteMessage(e, i)}>🚮</button>
                            <button className="action" onClick={(e) => this.editMessage(e, i)}>✏</button>
                        </div>
                        <p id="message-block-text">{this.messages.getMessage(i).text}</p>
                    </div>
                )
            } else {
                messagesList.push(
                    <div className="message-block">
                        <div className="messages-actions">
                            <img src={this.messages.getMessage(i).avatar} />
                            <span className="message-user-name">{this.messages.getMessage(i).user}</span>
                            <span className="message-date">{ currentHours > 10 ? currentHours : '0' + currentHours }:{ currentMinutes > 10 ? currentMinutes : '0' + currentMinutes }</span>
                            <button className="action" onClick={(e) => this.changeLiked(e, i)}>{this.messages.getMessage(i).liked ? '💔' : '💖'}</button>
                        </div>
                        <p id="message-block-text">{this.messages.getMessage(i).text}</p>
                    </div>
                )
            }


        }

        return messagesList;
    }

    parentUpdate() {
        this.forceUpdate();
    }

    render() {
        return (
            <div className="container">
                <ChatHeader />
                { this.createMessages() }
                <MessageCompose forceUpdate={() => this.parentUpdate()} />
            </div>
        )
    }

}

export default Chat;