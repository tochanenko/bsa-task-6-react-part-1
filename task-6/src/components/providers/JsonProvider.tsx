import SingleMessage from '../interfaces/SingleMessage';

class JsonProvider {
    public static getMessages(url: string): Promise<Array<SingleMessage>> {
        var messages = new Array<SingleMessage>();
        var promiseResolve: any;
        var promise = new Promise<SingleMessage[]>(resolve => {promiseResolve = resolve;});
 
        fetch(url).then(function(response) {
            return response.json();
        }).then(function(myJson) {
            for (var i = 0; i < myJson.length; i++) {
                messages.push({
                    id: myJson[i].id,
                    userId: myJson[i].userId,
                    avatar: myJson[i].avatar,
                    editedAt: myJson[i].createdAt,
                    text: myJson[i].text,
                    user: myJson[i].user,
                    liked: false
                } as SingleMessage);
                promiseResolve(messages);
            }
        });

        return promise;
    }
}

export default JsonProvider;