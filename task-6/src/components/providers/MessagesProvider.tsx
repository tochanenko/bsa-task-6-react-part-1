import SingleMessage from '../interfaces/SingleMessage';
import JsonProvider from './JsonProvider';

class MessageProvider {

    // constructor() {
    //     JsonProvider.getMessages('https://edikdolynskyi.github.io/react_sources/messages.json').then(messages => {
    //         this.messages = messages;
    //     });
    // }

    private static instance: MessageProvider;

    private messages: any;

    public setMessages(messages: Array<SingleMessage>) {
        this.messages = messages;
    }

    public static getInstance(): MessageProvider {
        if (!MessageProvider.instance) {
            this.instance = new MessageProvider();
        }

        return this.instance;
    }

    public addMessage(message: SingleMessage) {
        this.messages.push(message);
    }

    public getMessage(id: number): SingleMessage {
        return this.messages[id];
    }

    public getMessagesLength(): number {
        return this.messages.length;
    }

    public changeLiked(id: number) {
        this.messages[id].liked = !this.messages[id].liked;
    }

    public deleteMessage(id: number) {
        this.messages.splice(id, 1);
    }

    public editMessage(id: number, text: string, editedAt: string) {
        this.messages[id].text = text;
        this.messages[id].editedAt = editedAt;
    }

    public getLastMessageTime(): string {
        return this.messages[this.messages.length - 1].editedAt;
    }
}

export default MessageProvider;