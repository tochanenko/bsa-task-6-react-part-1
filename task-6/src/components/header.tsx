import React from 'react';
import PageHeader from './page-header';

class Header extends React.Component {
    render() {
        return (
            <div>
                <PageHeader />
            </div>
        );
    }
}

export default Header;