import React from 'react';
import '../resources/styles/page-header.css';

class PageHeader extends React.Component {
    render() {
        return (
            <div className='page-header'>
                <div className='container'>
                    <span>Telegram Soziales Netzwerk</span>
                </div>
            </div>
        );
    }
}

export default PageHeader;