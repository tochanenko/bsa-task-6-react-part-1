import React, { ChangeEvent } from 'react';
import '../resources/styles/message-compose.css';
import SingleMessage from '../components/interfaces/SingleMessage';
import MessagesProvider from '../components/providers/MessagesProvider';

class MessageCompose extends React.Component<{forceUpdate: () => void}, { message: string }> {

    constructor(props: any) {
        super(props);

        this.state = {
            message: 'Your message'
        };
    }

    private messages = MessagesProvider.getInstance();

    onChange(e: ChangeEvent<HTMLInputElement>) {
        const value = e.target.value;
        this.setState({
            message: value
        });
    }

    addMessage(e: React.MouseEvent) {
        e.preventDefault();
        this.messages.addMessage({
            id: 'random id',
            text: this.state.message,
            user: 'ME',
            avatar: 'https://i.redd.it/h5ey1njag4w41.jpg',
            userId: 'myid',
            editedAt: new Date().toString(),
            liked: false
        } as SingleMessage);
        this.props.forceUpdate();
    }

    render() {
        return (
            <div className="container">
                <div className="message-box">
                    <input type="text" placeholder={this.state.message} id="message" onChange={(e: ChangeEvent<HTMLInputElement>) => this.onChange(e)} />
                    <button onClick={(e) => this.addMessage(e)}>🔼Send</button>
                </div>
            </div >
        )
    }
}

export default MessageCompose;