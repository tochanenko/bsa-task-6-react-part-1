import React from 'react';
import '../resources/styles/footer.css';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div className="container">
                    <span>&copy; Vladislav Tochanenko</span>
                </div>
            </div>
        );
    }
}

export default Footer;