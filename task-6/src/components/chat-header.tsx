import React from 'react';
import '../resources/styles/chat-header.css';
import MessagesProvider from '../components/providers/MessagesProvider';

class ChatHeader extends React.Component<{}, { participants: string, messages: string, lastMessage: string }> {

    constructor(props: any) {
        super(props);

        var messagesProvider = MessagesProvider.getInstance();

        this.state = {
            participants: '23',
            messages: messagesProvider.getMessagesLength().toString(),
            lastMessage: messagesProvider.getLastMessageTime().toString()
        }
    }

    render() {
        var messagesProvider = MessagesProvider.getInstance();

        this.state = {
            participants: '23',
            messages: messagesProvider.getMessagesLength().toString(),
            lastMessage: messagesProvider.getLastMessageTime().toString()
        }

        var currentFullDate = new Date(this.state.lastMessage);
        var currentHours = currentFullDate.getHours();
        var currentMinutes = currentFullDate.getMinutes();

        return (
            <div className='container'>
                <div className='chat-header'>
                    <div className='leftie'>My chat</div>
                    <div className='leftie'>{ this.state.participants } participants</div>
                    <div className='leftie'>{ this.state.messages } messages</div>
                    <div className='rightie'>Last message at { currentHours > 10 ? currentHours : '0' + currentHours }:{ currentMinutes > 10 ? currentMinutes : '0' + currentMinutes } </div>
                </div>
            </div>
        );
    }
}

export default ChatHeader;