interface IUser {
    firstName: string;
    secondName?: string;
}

var num: number = 10;
console.log(num);

function getData(): [number, string] {
    return [6, 'hello'];
}

let user = {
    firstName: 'John',
    lastName: 'Peter'
}

console.log(getData());