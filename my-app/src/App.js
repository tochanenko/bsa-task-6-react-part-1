import React from 'react';
import logo from './logo.svg';
import './App.css';
import UserForm from './components/UserFormWithValidation'

function App() {
    const data = {
        checked: false,
        name: 'Edik',
        email: 'edik@gmail.com',
        password: '12345'
    };

    return (
        <div className="App">
            <UserForm data={data} />
        </div>
    )
}

export default App;
