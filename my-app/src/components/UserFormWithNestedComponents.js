import React from 'react';
import TextInput from './inputs/TextInput';
import PasswordInput from './inputs/PasswordInput';

class UserForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Edik',
            email: 'edik@gmail.com',
            password: '12345'
        };
        this.onChange = this.onChange.bind(this);
    }

    onChange(e, keyword) {
        const value = e.target.value;
        this.setState({
            ...this.state,
            [keyword]: value
        });
    }

    render() {
        const data = this.state;

        return (
            <div>
                <TextInput
                    label={'name'}
                    type={'text'}
                    text={data.name}
                    keyword={'name'}
                    onChange={this.onChange}
                />
                <TextInput
                    label={'email'}
                    type={'text'}
                    text={data.email}
                    keyword={'email'}
                    onChange={this.onChange}
                />
                <PasswordInput
                    label={'password'}
                    type={'password'}
                    text={data.password}
                    keyword={'password'}
                    onChange={this.onChange}
                />
            </div>
        );
    }
}

export default UserForm;