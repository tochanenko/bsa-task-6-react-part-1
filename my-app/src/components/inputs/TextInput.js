import React from 'react';

class TextInput extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        
    }

    render() {
        const props = this.props;

        return (
            <div>
                <label>{props.label}</label>
                <input
                    value={props.text}
                    type={props.type}
                    onChange={e => props.onChange(e, props.keyword)}
                />
            </div>
        );
    }
}

export default TextInput;
